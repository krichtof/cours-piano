---
layout: journal
title: Journal
permalink: /journal.html
---

A la fin de chaque cours, nous consignons ici ce que nous avons appris et ce que nous comptons faire d'ici le cours prochain.
