---
layout: page
title: Cours de piano
---

# Présentation

Je joue du piano depuis l'âge de 6 ans. Et j'ai envie de vous transmettre ma passion. Tout simplement.

Je prends le pari qu'on peut apprendre le piano sans au préalable connaître le solfège.

Nous travaillerons à partir de vos goûts musicaux. Plutôt que de reproduire mécaniquement une partition, nous essaierons d'aiguiser votre oreille, votre sens du rythme, et votre mémoire.

Un journal sera tenu pour consigner les objectifs qu'on se fixe, ce que chacun a appris, et ce qu'on souhaite travailler pour la fois suivante.

# Biographie

Élevé au piano classique, ma crise d’adolescence me pousse à claquer la porte du conservatoire pour me lancer à corps perdu dans le piano-jazz. J'écume alors les pianos-bars de la Seine-et-Marne (notamment celui de la Ferme du Buisson, scène nationale) et de Seine-Saint-Denis.
 
 Quand j'arrive sur Paris, c’est vers l’accompagnement de spectacles d’improvisation théâtrale que mes premiers pas me mènent. Je deviens ainsi notamment l’impro-musicien des spectacles Improvisafond dirigé par Michel Lopez, Impro Ex Machina et Histoires d’un soir.
 
Je lance ensuite mon projet personnel, Robi & Co, avec lequel j'égrène [mes balades aérées et entrainantes](https://soundcloud.com/robiandco/sets/pommes-de-terre), des reprises décalées et des chansons improvisées. J'en donnerai un aperçu sur le plateau de Vivement Dimanche, où j'interprèterai "Derrière les cimetières" pour rejoindre ensuite [le canapé rouge de Michel Drucker](https://www.youtube.com/watch?v=3s6wtXFY4H8)...

Et maintenant qu'on se connaît un peu (!), je dois vous avour qu'il m'arrive aussi de faire [d'autres trucs](https://christophe.robiweb.net) que des cours de piano...
