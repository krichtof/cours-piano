---
layout: post
date:   2021-04-27
categories: journal matthieu
---
## Objectifs

- initiation au piano : jouer en accords
- travailler la composition (thème : rock psyché)
- si je sais chanter, je serai ravi


- apprendre le solfège, parce que hypothèse que si je fais du solfège, je serai un peu meilleur
- bosser l'harmonie
- progresser : enrichir le jeu. Trouver des meilleurs rythmiques.



# Ce qu'on a appris

- M : chanter la melodie est essentielle pour improviser, et je pense même pour jouer
- M : je dois travailler mon oreille. Quand j'écoute une fois un accord mineur puis majeur, j'arrive ensuite à reconnaitre
- M : j'ai appris ce qu'était un renversement : jouer les notes dans un ordre différent de l'ordre classique
- M : j'ai appris à commencer à jouer du piano
- M : j'ai appris à reconnaitre une note sur le clavier.
- C : exercie pour visualiser clavier (séparer en 2 blocs : de do à mi, de fa à do)
- C : mineur/majeur plus facile à reconnaitre d'abord relativement, et seulement après de manière absolue

# Nos  souhaits

- M : J'aimerai travailler un morceau et le décortiquer techniquement (la chanson de Prévert)
- M : J'aimerai travailler un morceau des Beatles (sauf Yesterday et Let it Be)
- C : clarifier ma politique de prix
- C : préparer des exercices en rapport avec les souhaits de M

# Le plan d'ici mardi prochain

- M : reconnaitre toutes les notes sur le piano
- M : travailler chaque accord avec différents renversements : Am C F C G C
- C : demander à Karim et à Antoine comment ils procèdent pour donner des indications avant que l'utilisateur choisisse le prix qu'il souhaite donner
- C : trouver un·e prof de piano et échanger sur les méthodes pedagogiques
- C : échanger avec Yannick sur le cours de piano, le journal
- C : publier le journal de bord

Rdv Mardi 19h. Souplesse si pas possible pour l'un ou pour l'autre, on trouvera un autre créneau dans la semaine

