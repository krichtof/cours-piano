---
layout: post
date:   2021-06-09
categories: journal enoch
---
# Ojbectifs

- je trouve ça beau à l'oreille le piano. C'est grâcieux.
- ça peut me servir pour rendre les gens heureux


# Ce qu'on a fait, ce qu'on a appris

- c'était trop bien. De savoir que c'est moi qui a joué la mélodie, c'est satisfaisant.
- il y a eu beaucoup d'informations
- le plus compliqué, c'est quand il fallait jouer les 2 mains en même temps
- dans un octave, il y a do re mi fa sol la si do.
- quand il y a  2 touches noires, avant la 1ere noire, c'est do. Avant les 3 noires, c'est fa.
- le dièse, c'est un demi-ton au-dessus d'une note. Le bémol, c'est un demi-ton au-dessous de la note

# Pour la prochaine fois

- jouer do-re-mi-fa-sol-fa-mi (1 2 3 4 5 4 3)
- jouer le morceau inventé les mains séparément :
  - main droite : mi do mi fa
  - main gauche : do sol do fa
