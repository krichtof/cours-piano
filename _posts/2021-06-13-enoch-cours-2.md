---
layout: post
date:   2021-06-13
categories: journal enoch
---

# Ce qu'on a fait, ce qu'on a appris

- je suis épuisé mais fier de moi !
- j'ai rejoué la premiere partie du morceau que j'ai composé. Et j'ai réussi
- On a inventé une suite :
  - main droite : mi sol sol sol mi sol sol sol fa
  - main gauche : do do fa
  - 
- On a travaillé la montée : do re mi fa sol fa mi - re mi fa sol la sol fa - mi - fa - sol - la - si à la main gauche et la main droite
- Cet exercice avec les deux mains m'a grillé le cerveau !

# Pour la prochaine fois

- je travaille le morceau composé chaque main séparément
- puis si possible avec les deux mains
- je travaille la montée chaque main séparément
- puis si possible avec les deux mains
