---
layout: post
categories: journal matthieu
---

# Ce qu'on a fait / appris

- descente et montée de la gamme de do
  - montée : main droite : 1 2 3 4 5 4 3 1 2 3 4 5 4 3  / main gauche : 5 4 3 2 1 2 3 5 4 3 2 1 2 3)
  - descente : main droite : 5 4 3 2 1 2 3 5 4 3 2 1 2 3 / main gauche : 1 2 3 4 5 4 3 1 2 3 4 5 4 3)
  - les deux mains ensemble. Compliqué de bien positionner la main. D'arriver à ne pas lever l'auriculaire et garder la main droite perpendiculaire au piano.

- on a travaillé la mélodie de la chanson de Prévert. En essayant de trouver à chaque fois le bon doigté. A savoir, celui qui permet de déplacer le moins la main et d'avoir toutes les notes dans les doigts.

- on a bossé les accords de la chanson de Prévert à la main gauche, avec tous les renversements possibles.
- on a exploré deux méthodes pour trouver les accords :
  -  de manière théorique en calculant le nombre de tons entre chaque note de l'accord
  -  de manière "visuelle" : avec la gamme de do, on a do majeur, re mineur, mi mineur, fa majeur, sol majeur, la mineur, si diminué. Et à partir de ces accords, on peut trouver facilement le mi majeur, le sol mineur etc...

- ça demande pas mal de réflexion de trouver l'accord suivant de la chanson, en déplaçant le moins possible la main et les doigts.


# Le plan d'ici 2 semaines

- travailler les enchainements d'accords
- travailler la mélodie de la chanson de prévert lentement en se concentrant sur la position des mains et des doigts (trouver le bon doigté, garder les doigts sur les touches, déplacer le moins possible la main qui doit rester perpendiculaire au piano)
- la montée et descente. Chaque main séparément.
- la montée, les deux mains en même temps

Rdv jeudi 17/06 à 13h30

