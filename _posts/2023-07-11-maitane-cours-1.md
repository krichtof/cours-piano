---
layout: post
date:   2023-07-11
categories: journal maitane
---

## Objectif

- jouer des chansons de dessin animé

## Ce que j'aime écouter

- dessins animés pokemon
- disney reine des neiges

## Ce que j'ai appris, ce que j'ai fait

J'ai travaillé [pokemon](https://www.youtube.com/watch?v=jVm1NbrXaXc) jusqu'à 0.35 (avant le 1er refrain)
- appuyer avec un petit doigt
- enchainer les touches
- poignet ne bouge pas, avec main perpendiculaire au piano
- j'ai joué avec les deux mains

## Ce qui n'est pas évident

Je n'arrive pas à faire le lien entre la musique que j'entends et ce que je joue.

## pour la prochaine fois

(travailler au metronome)
- je joue la main gauche seule au tempo lent
- je joue la main droite seule au tempo lent
- les 2 mains tempo lent





