---
layout: post
categories: journal matthieu
---

# Ce qu'on a fait / appris

- descente et montée de la gamme de do
  - montée : main droite : 1 2 3 4 5 4 3 1 2 3 4 5 4 3  / main gauche : 5 4 3 2 1 2 3 5 4 3 2 1 2 3)
  - descente : main droite : 5 4 3 2 1 2 3 5 4 3 2 1 2 3 / main gauche : 1 2 3 4 5 4 3 1 2 3 4 5 4 3)
  - les deux mains ensemble. Cela devient compliqué. Cela demande beaucoup de réflexion pour que ce soit fluide.
  
  - bien penser de positionner les doigts sur la touche à chaque décalage

- identifier à l'oreille une quarte et une quinte. Petit rappel : les deux premières notes du couplet de l'Internationale ("C'est nous"), c'est une quarte !
- identifier à l'oreille si un accord est majeur ou mineur

- Chanson de Prévert.

On a travaillé à partir du renversement do mi la.
Pour bien décomposer la main droite qui fait les accords, regarder dans l'enchainement les notes qui changent, travailler l'enchainement uniquement de ces notes, puis rajouter la 3e.

# Le plan d'ici mardi prochain

- travailler les enchainements d'accords
- la montée et descente. Chaque main séparément.
- la montée, les deux mains en même temps

Rdv jeudi 19h.

