---
layout: post
date:   2021-06-04
categories: journal mahin
---

# Ce qu'on a fait

- on a travaillé à partir d'une mélodie inventée par Mahin
- on a fait la main droite et la main gauche séparément.
- on a réussi un peu à faire les 2 mains en même temps. 
  la main gauche, avec 4 doigts (en fait, 5,4, 2), c'est compliqué

## Pour la prochaine fois

- tu travailles la main droite seule, en pensant au poignet qui ne bouge pas, et aux doigts qui enfoncent juste les notes puis restent sur le clavier
- tu travailles la main gauche seule, en pensant au poignet qui ne bouge pas, et aux doigts qui enfoncent juste les notes puis restent sur le clavier
- tu t'entraines à faire avec les deux mains
