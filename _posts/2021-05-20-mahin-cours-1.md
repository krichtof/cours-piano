---
layout: post
date:   2021-05-21
categories: journal mahin
---
## Objectifs

- c'est mon instrument préféré
- pour le plaisir d'en faire.
- faire des chansons

#

- je me sens bien
- quand ça monte, c'est plus aigu, quand ça descent, c'est plus grave
- le clavier est composé de 2 notes noires, plus 3 notes noires, puis 2 notes noires, puis 3 notes noires et tout plein de notes blanches
- pour trouver un dos, on cherche 2 notes noires, et le dos est juste avant la première note noire
- pour trouver un fa, on cherche 3 notes noires, et le fa est juste avant la première note noire

- la mélodie, c'est ce qu'on chante. On la joue avec la main droite.
- on a commencé à travailler "A vous la terre". Main droite : ça commence par le la, 1 2 3 2 1
- la main gauche pour accompagner : 4 - 5
- bien penser quand on joue les notes de relever le doigt quand je change de note
## Chansons que j'aime

- Rallumeur d'étoiles - HK
- 3 cafés gourmands - Papayak
- On a tous - Victor et le Ukulélé - Kandid
- A vous la terre - Les ogres de barback

