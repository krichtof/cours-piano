---
layout: post
date:   2023-07-09
categories: journal louise
---

## Petite présentation

- Je sais jouer avec accords
- Je sais lire une partition, mais ne sais pas déchiffrer en temps réel
- J'ai des [boomwhackers](https://boomwhackers.com/) !

### Mes attentes

Qu'est-ce que je peux faire pour qu'un la mineur sonne bien ?
Comment bien m'accompagner ?

### Ce que j'écoute :

- Tom Waits
- Lou Reed
- Buena Vista
- David Bowie
- Leonard Cohen
- Mamas & Papas
- Goldman
- Renaud
- Gainsbourg
- Mongo Jerry (in the summertime)



## Ce que j'ai appris

- J'ai appris les renversements d'accord
- J'ai appris à enchainer les accords de manière habile, de manière à ce qui y ait le moins de mouvement possible de la main

## Ce que j'aimerai

- introduire une rythmique plus riche

## Exo pour la prochaine fois

- maitriser lou reed en commençant par mi sol do
  - avec en basse la fondamentale
  - en doublant à l'ocatve la fondamentale
- en commançant par sol do mi

## Exo pour Emile

- ecouter la partie de batterie honky tonk women
- reproduire la partie batterie. En mettre le moins possible
