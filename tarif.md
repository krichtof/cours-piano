---
layout: page
title: Tarif
---

Je pratique le [prix libre](https://fr.wikipedia.org/wiki/Prix_libre)

"Le prix libre est un mode de fixation du prix par l'acheteur d'un bien ou d'un service."
"Le prix libre peut être mis en place à des fins de justice sociale : il permet de payer un prix qu'on estime juste en fonction de ses moyens, pour accéder à un bien qui aurait été trop coûteux lorsque ses moyens sont très limités, ou pour permettre à d'autres d'en bénéficier en finançant plus sa production ou sa diffusion."

J'accepte les € ou la [monnaie libre Ğ1](https://monnaie-libre.fr/)

